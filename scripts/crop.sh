#!/bin/bash

set -o errexit -o pipefail -o noclobber -o nounset

# requires ffmpeg

# This has to be determined per input video (input parameters)
# 1920:1080 -> 43
# 1680:1050 -> 40

PIXEL_PER_TILE=39

# Output parameters 
INP_FILE=$1
OUT_FILE=$2

TILES_UP=5
TILES_DOWN=6
TILES_LEFT=5
TILES_RIGHT=5
START=0
DURATION=3

# Get video dimensions
DIMENSIONS=$(ffprobe -v error -show_entries stream=width,height -of default=noprint_wrappers=1 ${INP_FILE})
eval ${DIMENSIONS^^} # upper case variable names: WIDTH, and HEIGHT

echo $WIDTH $HEIGHT

# cut videos
OUT_WIDTH=$(( ($TILES_LEFT + $TILES_RIGHT + 1) * $PIXEL_PER_TILE ))
OUT_HEIGHT=$(( ($TILES_UP + $TILES_DOWN + 1) * $PIXEL_PER_TILE ))
# calculate top left pixel
OUT_X=$(( ($WIDTH - $PIXEL_PER_TILE) / 2 - $TILES_LEFT * $PIXEL_PER_TILE))
OUT_Y=$(( ($HEIGHT - $PIXEL_PER_TILE) / 2 - $TILES_UP * $PIXEL_PER_TILE))

# half the speed
#ffmpeg -i ${INP}.mkv -filter:v "setpts=0.5*PTS" ${OUT}

ffmpeg -i $INP_FILE \
	-vf "crop=${OUT_WIDTH}:${OUT_HEIGHT}:${OUT_X}:${OUT_Y}" \
	$OUT_FILE
	#-ss $START -t $DURATION \


